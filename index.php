<?php

$loader = require_once 'vendor/autoload.php';

require 'framework/Kernel.php';

use Framework\Kernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

$request = Request::createFromGlobals();

$kernel = new Kernel();

// read the config/routes.yaml file and register the routes
$yaml = new Yaml();
$parsedRoutes = $yaml::parseFile('config/routes.yaml');

foreach ($parsedRoutes as $alias => $routeAttributes) {
    $kernel->map($routeAttributes['path'], $routeAttributes['controller'], $routeAttributes['methods']);
}

$response = $kernel->handle($request);

$response->send();
