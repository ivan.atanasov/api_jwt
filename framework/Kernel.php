<?php

namespace Framework;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Kernel implements HttpKernelInterface
{
    /** @var RouteCollection */
    protected $routes;

    public function __construct()
    {
        $this->routes = new RouteCollection();
    }

    public function handle(Request $request, int $type = self::MASTER_REQUEST, bool $catch = true)
    {
        $context = new RequestContext();
        $context->fromRequest($request);

        $matcher = new UrlMatcher($this->routes, $context);

        try {
            $attributes = $matcher->match($request->getPathInfo());
            $controller = $attributes['controller'];
            unset($attributes['controller']);
            $content = call_user_func_array($controller, $attributes);
            $response = (new Response($content))->setStatusCode(Response::HTTP_OK);
        } catch (ResourceNotFoundException $e) {
            $response = (new Response('Path not found'))->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        return $response;
    }

    public function map(string $path, $controller, string $httpMethod)
    {
        $this->routes->add($path, new Route($path, ['controller' => $controller], [], [], '', [], $httpMethod));
    }
}