<?php

namespace Api\Controller;

class ProductController
{
    public function getAll(): string
    {
        return json_encode([
            [
                ['title' => 'Lord of the rings mug', 'price' => 1205],
                ['title' => 'Funko pop figure', 'price' => 2300],
            ]
        ]);
    }
}